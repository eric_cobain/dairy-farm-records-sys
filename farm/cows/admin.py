from django.contrib import admin
from cows.models import Cow, Breed, Herd_Book, HealthStatus, ServiceDate, CalvingDate


class CowAdmin(admin.ModelAdmin):
    prepopulated_fields   = {'slug':('ear_tag',)} 
    list_display          = ('ear_tag','reg_name', 'sex','breed', 'herd_book')
    search_fields         = ['reg_name'] 

class BreedAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug':('name',)}


class Herd_BookAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug':('name',)}





admin.site.register(Cow, CowAdmin)
admin.site.register(Breed, BreedAdmin)
admin.site.register(Herd_Book, Herd_BookAdmin)
admin.site.register(HealthStatus)
admin.site.register(ServiceDate)
admin.site.register(CalvingDate)




