from django import forms
from django.forms import ModelForm
from cows.models import Cow, Breed, Herd_Book, ServiceDate, CalvingDate, HealthStatus, Sex_choices 


class RegisterCowForm(ModelForm):
    ear_tag     = forms.CharField(label=(u'Ear Tag'))
    reg_name    = forms.CharField(label=(u'Reg Name'))
    cow_id      = forms.CharField(label=(u'ID'))
    breed       = forms.ModelChoiceField(queryset=Breed.objects.all())
    herd_book   = forms.ModelChoiceField(queryset=Herd_Book.objects.all())
    sex         = forms.CharField(max_length=1,widget=forms.Select(choices=Sex_choices))
    grade       = forms.CharField(label=(u'grade'))
    birthday    = forms.DateField()
    status      = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = Cow
        exclude = ('herd_book','breed')

    def clean_ear_tag(self):
        ear_tag = self.cleaned_data['ear_tag']
       	try:
       	    Cow.objects.get(ear_tag=ear_tag)
       	except Cow.DoesNotExist:
       	    return ear_tag
       	raise forms.ValidationError("Sorry,that ear_tag has already being registered in the database.")

    



