from django.db import models
from django.db.models.signals import post_save

Sex_choices = (('M', 'Male'),('F','Female'),)

class Cow(models.Model):
    ear_tag     = models.CharField(max_length=100)
    reg_name    = models.CharField(max_length=100)
    slug        = models.SlugField(unique=True)
    cow_id      = models.CharField(max_length=100)
    breed       = models.ForeignKey('Breed')
    herd_book   = models.ForeignKey('Herd_Book')
    sex         = models.CharField(max_length=1, choices=Sex_choices)
    grade       = models.CharField(max_length=100)
    birthday    = models.DateField()
    status      = models.TextField(blank=True)
	
    def __unicode__(self):
	   return self.reg_name

       



class Breed(models.Model):
    name        = models.CharField(max_length=100)
    slug        = models.SlugField(unique=True)
    description = models.TextField(blank=True)
	
    def __unicode__(self):
	   return self.name


class Herd_Book(models.Model):
    name        = models.CharField(max_length=100)
    slug        = models.SlugField(unique=True)
    description = models.TextField(blank=True)

    def __unicode__(self):
	   return self.name 

class HealthStatus(models.Model):
    cow         = models.ForeignKey('Cow')
    date        = models.DateField(auto_now=True)
    status      = models.TextField()

class ServiceDate(models.Model):
    cow             = models.ForeignKey('Cow')
    date            = models.DateField()
    description     = models.TextField()

class CalvingDate(models.Model):
    cow             = models.ForeignKey('Cow')
    date            = models.DateField()
    note            = models.TextField(blank=True)                 



