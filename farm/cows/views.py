from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template  import RequestContext
from cows.models import Cow, Breed, Herd_Book, ServiceDate, CalvingDate, HealthStatus 
from cows.forms import RegisterCowForm



def All_Cows(request):
    cows        = Cow.objects.all().order_by('cow_id')
    context     = {'cows':cows}
    return render_to_response('allcows.html',context,context_instance=RequestContext(request)) 


def Specific_Cow(request, cowslug):
    cow             = Cow.objects.get(slug=cowslug)
    health_status   = HealthStatus.objects.all().filter(cow=cow).order_by('-date')
    service_dates   = ServiceDate.objects.all().filter(cow=cow).order_by('-date')
    calving_dates    = CalvingDate.objects.all().filter(cow=cow).order_by('-date')
    context         = {'cow': cow, 'health_status':health_status, 'service_dates':service_dates, 'calving_dates':calving_dates}
    return render_to_response('cow.html', context, context_instance=RequestContext(request))


def Specific_Breed(request, breedslug):
    breed       = Breed.objects.get(slug=breedslug)
    cows        = Cow.objects.filter(breed=breed)
    context     = {'cows':cows, 'breed':breed}
    return render_to_response('breed.html', context, context_instance=RequestContext(request))    


def Specific_Herd_Book(request, herdbookslug):
    herd_book    = Herd_Book.objects.get(slug=herdbookslug)
    cows         = Cow.objects.filter(herd_book=herd_book)
    context      = {'cows':cows, 'herd_book':herd_book}
    return render_to_response('herd_book.html', context, context_instance=RequestContext(request))


def Service_Dates(request):
    service_dates =   HealthStatus.objects().order_by('date')
    context = {'service_dates':service_dates}

def All_Records(request):
    cows        = Cow.objects.all().order_by('cow_id')
    health_status   = HealthStatus.objects.all().order_by('-date')
    service_dates   = ServiceDate.objects.all().order_by('-date')
    calving_dates    = CalvingDate.objects.all().order_by('-date')
    breeds       = Breed.objects.all()
    herd_books    = Herd_Book.objects.all()
    context = {'cows': cows, 'health_status':health_status,'breeds':breeds, 'herd_books':herd_books,
                'service_dates':service_dates, 'calving_dates':calving_dates}
    return render_to_response('records.html', context, context_instance=RequestContext(request))
            




def HomePage(request):
    cows        = Cow.objects.all().order_by('ear_tag')
    context     = {'cows':cows}
    return render_to_response('home.html',context,context_instance=RequestContext(request))

def Cow_Registration(request):
    if request.method == 'POST':
        form = RegisterCowForm(request.POST)
        if form.is_valid():
            cow = Cow(ear_tag = form.cleaned_data['ear_tag'],
                                         slug=form.cleaned_data['slug'],   
                                         reg_name = form.cleaned_data['reg_name'], 
                                        cow_id = form.cleaned_data['cow_id'],
                                        breed=form.cleaned_data['breed'],
                                        sex=form.cleaned_data['sex'],
                                        grade=form.cleaned_data['grade'],
                                        herd_book=form.cleaned_data['herd_book'],
                                        birthday=form.cleaned_data['birthday'],
                                        status=form.cleaned_data['status'])
            cow.save()
            return HttpResponseRedirect('/cows/')
        else:
            return render_to_response('register.html', {'form':form}, context_instance=RequestContext(request))        
    else:
        ''' form not being submitted, show them the blank registration form  for submission'''
        form  = RegisterCowForm()
        context = {'form':form}
        return render_to_response('register.html', context, context_instance=RequestContext(request))
                












    