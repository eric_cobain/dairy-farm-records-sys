from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    ('^$', 'cows.views.HomePage'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    ('^cows/$','cows.views.All_Cows'),
    ('^records/$', 'cows.views.All_Records'),
    ('^register/$', 'cows.views.Cow_Registration'),
    ('^cows/(?P<cowslug>.*)/$', 'cows.views.Specific_Cow'),
    ('^breeds/(?P<breedslug>.*)/$', 'cows.views.Specific_Breed'),
    ('^herdbook/(?P<herdbookslug>.*)/$', 'cows.views.Specific_Herd_Book'),
)
